#
#  Makefile for the DHS source tree.
#
# ---------------------------------------------------------------------------

# Compiler Flags.

RELEASE		= v1.0

CC 		= gcc
AS 		= gcc -c -x assembler
AR 		= ar clq
CP 		= cp -p

CFLAGS 		= -O2 -Wall -m32
CDEBUGFLAGS 	= -O2 -Wall -m32 -g
BOOTSTRAPCFLAGS = -O2  -pipe -march=i386 -mcpu=i686 -pipe
        

#LIBDIRS 	= lib/dcalib lib/dhsCmds lib/dhslib lib/mbus lib/smCache
#APPDIRS 	= test super collector pxf smcmgr mosdca
#SUBDIRS 	= $(LIBDIRS) $(APPDIRS)

all: update

update:
	@echo "Updating the DHS $(RELEASE) software tree"
	@echo "" ; date ; echo ""
	@echo ""
	@echo "static char *build_date = \""`date`"\";"  > build.h
	@echo "BEFORE libs"
	$(MAKE) $(MFLAGS) libs
	@echo "AFTER libs"
	$(MAKE) $(MFLAGS) apps
	@echo "AFTER apps"
#	$(MAKE) $(MFLAGS) install
	@echo "AFTER install"
	@echo "" ; date ; echo ""
	@echo "Update Done."


libs:
	$(MAKE) $(MFLAGS) dcalib
	$(MAKE) $(MFLAGS) dhslib
	$(MAKE) $(MFLAGS) dhsCmds
	$(MAKE) $(MFLAGS) mbus
	$(MAKE) $(MFLAGS) smCache

# need "do_" to avoid name collisions with subdirectories
apps:
	$(MAKE) $(MFLAGS) do_super
	$(MAKE) $(MFLAGS) do_collector
	$(MAKE) $(MFLAGS) do_smcmgr
	$(MAKE) $(MFLAGS) do_pxf
#	$(MAKE) $(MFLAGS) do_mosdca
	$(MAKE) do_mosdca
	$(MAKE) $(MFLAGS) do_test

do_super:
	(cd super ; echo "making all in super ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
do_collector:
	(cd collector ; echo "making all in collector ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
do_smcmgr:
	(cd smcmgr ; echo "making all in smcmgr ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
do_pxf:
	(cd pxf ; echo "making all in pxf ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);

#	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
do_mosdca:
	(cd mosdca ; echo "making all in mosdca ...";\
	 	$(MAKE) install);

do_test:
	(cd test ; echo "making all in test ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);


dcalib:
	(cd lib/dcalib ; echo "making all in lib/dcalib ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
dhslib:
	(cd lib/dhslib ; echo "making all in lib/dhslib ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
dhsCmds:
	(cd lib/dhsCmds ; echo "making all in lib/dhsCmds ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
mbus:
	(cd lib/mbus ; echo "making all in lib/mbus ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);
smCache:
	(cd lib/smCache ; echo "making all in lib/smCache ...";\
	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' install);

# x11iraf:
# 	(cd x11iraf ; echo "making all in x11iraf ...";\
# 	 	$(MAKE) $(MFLAGS) 'CDEBUGFLAGS=$(CDEBUGFLAGS)' World);


patch:
	(tar -czf dtp.tgz */*.[ch] */*/*.[ch])


archive:
	#$(MAKE) $(MFLAGS) pristine
	@(tar -cf - . | gzip > ../dts-$(RELEASE)-src.tar.gz)

#pristine:
	#$(MAKE) $(MFLAGS) cleandir
	#$(MAKE) $(MFLAGS) generic
	#$(RM) -rf bin.[a-fh-z]* lib.[a-fh-z]* bin.tar* include *spool* \
	#	Makefile makefile Makefile.bak */Makefile */Makefile.bak \
	#	**/Makefile.bak
	#$(RM) -f   bin/*
	#$(RM) -rf  lib/*

emptyrule:

cleandir:
	(cd lib/dcalib  ; make clean)
	(cd lib/dhslib  ; make clean)
	(cd lib/dhsCmds ; make clean)
	(cd lib/mbus    ; make clean)
	(cd lib/smCache ; make clean)
	(cd super       ; make clean)
	(cd collector   ; make clean)
	(cd smcmgr      ; make clean)
	(cd pxf         ; make clean)
	(cd mosdca      ; make clean)
	(cd test        ; make clean)
	$(RM) *.CKP *.ln *.BAK *.bak *.o core errs ,* *~ *.a .emacs_* tags TAGS make.log MakeOut   "#"*
	$(RM) -rf   bin/super bin/collector bin/smcmgr bin/pxf bin/mosdca bin/smcop
	$(RM) -rf   bin/exp bin/pan bin/nocs bin/dhscmd
	$(RM) -rf   lib/libdcalib.a lib/libdhslib.a lib/libdhsCmds.a
	$(RM) -rf   lib/libmbus.a lib/libsmCache.a

tags:
	$(TAGS) -w *.[ch]
	$(TAGS) -xw *.[ch] > TAGS


clean: cleandir

distclean: cleandir

# install:
# 	@for flag in ${MAKEFLAGS} ''; do \
# 	case "$$flag" in *=*) ;; --*) ;; *[ik]*) set +e;; esac; done; \
# 	for i in $(SUBDIRS) ;\
# 	do \
# 	echo "installing" "in $(CURRENT_DIR)/$$i..."; \
# 	$(MAKE) -C $$i $(MFLAGS) $(PARALLELMFLAGS) DESTDIR=$(DESTDIR) install; \
# 	done

