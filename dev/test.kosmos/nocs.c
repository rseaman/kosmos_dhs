#if !defined(_dhsUtil_H_)
#include "dhsUtil.h"
#endif
#if !defined(_dhsImpl_H_)
#include "dhsImplementationSpecifics.h"
#endif

#include "dcaDhs.h"
#include "mbus.h"
#include "nocsHdr.h"

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>


#ifndef OK
#define OK 	1
#endif
#ifndef IamPan
#define IamPan 	1
#endif
#ifndef IamNOCS
#define IamNOCS 2
#endif

#define N_PANS	1


struct timeval tv;
struct timezone tz;

int sim 	= 0;
int sky 	= 0;
int loop 	= 1;
int delay 	= 0;
int interactive = 0;
int use_nocs 	= 0;
int lbnl 	= 0;

int nbin1               = 1;
int nbin2               = 1;
int x_roi               = 2048;
int y_roi               = 4096;

int procDebug;

char 	*pTime();

static char *trim (char *buf);


int main(int argc, char *argv[])
{
    int   blkSize, ncols=2148, nrows=4096, nkeys=0;
    int   xstart=0, ystart=0;
    int   i, nscan, mytid, n_pans = N_PANS;
    int   from_tid, to_tid, subject, *p = NULL;

    char  resp[80], retStr[80], mdBuf[32776];
    char  *op, *obsID = "TestID";
    char   keyw[32], val[32], comment[64], buf[80];
    char   nocroipt[32];
    char   rcode[SZ_LINE], bcode[SZ_LINE];
    char  *host, *msg;
    XLONG   istat;
    double expID;

    mdConfig_t mdCfg;
    fpConfig_t fpCfg = {0L};
    dhsHandle dhsID = 0;
    dhsHandle nocs_dhsID;



    /* Process the command-line arguments.
     */
    for (i = 1; i < argc; i++) {
	if (strncmp(argv[i], "-help", 5) == 0) {
	    /*clientUsage (); */
	    exit(0);;

	} else if (strncmp(argv[i], "-delay", 5) == 0) {
	    delay = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-debug", 5) == 0) {
	    procDebug = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-npans", 3) == 0) {
	    n_pans = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-interactive", 5) == 0) {
	    interactive++;
	    loop += 999;

        } else if (strncmp(argv[i], "-host", 5) == 0) {
            dcaInitDCAHost ();       		/* set simulated host */
            dcaSetSimHost (argv[++i], 1);

        } else if (strncmp(argv[i], "-sim", 4) == 0) {
	    /* dcaSetSimMode (1); */
	    sim++;

        } else if (strncmp(argv[i], "-sky", 4) == 0) {
	    /* If set, every other frame will be a NOCTYP=sky frame.
	    */
	    sky++;

	} else if (strncmp(argv[i], "-loop", 5) == 0) {
	    loop = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-lbnl", 5) == 0) {
	    lbnl = 1;
	    ncols = 4198;	
	    nrows = 4096;

	} else if (strncmp(argv[i], "-no_nocs", 8) == 0) {
	    use_nocs = 0;

	} else {
	    /*clientUsage (); */
	    exit(0);;
	}
    }


    /* Initialize connections. To the supervisior  */
    /* dhsHandle is a structure */
    if ((mytid = mbusConnect("CMD", "NOCS", FALSE)) <= 0) {
	fprintf(stderr, "ERROR: Can't connect to message bus.\n");
	exit(1);
    }

    /* initialize global values */
    dhs.expID = 0;
    dhs.obsSetID = "";


    /*  Simulate a SysOpen from the NOCS.
     */
    istat = 0;
    printf("=================start dhsSysOpen=================\n");
    dhsSysOpen(&istat, resp, (dhsHandle *) & nocs_dhsID, (XLONG) IamNOCS);
    if (istat != DHS_OK) {
	fprintf(stderr, "ERROR: dhsSysOpen failed. \\\\ %s\n", resp);
	exit(1);
    }
    printf("================= end dhsSysOpen =================\n");


    printf
	("=================start nocs dhsOpenConnect=================\n");
/*	xstart = (2048 - x_roi) / 2;
 *	ystart = (4096 - y_roi) / 2;
 *	xsize  = x_roi / nbin1 + 100;
 *	ysize  = y_roi / nbin2;
 */

/*    fpCfg.xStart = (XLONG) 864;
 *    fpCfg.yStart = (XLONG) 0;
 *    fpCfg.xSize = (XLONG) 260;
 *    fpCfg.ySize = (XLONG) 2048;
 */

/*    fpCfg.xStart = (XLONG) 512;
 *    fpCfg.yStart = (XLONG) 1536;
 *    fpCfg.xSize = (XLONG) 612;
 *    fpCfg.ySize = (XLONG) 1024;
 */

    /* provide an unbinned full frame default
     */
    fpCfg.xStart = (XLONG) 0;
    fpCfg.yStart = (XLONG) 0;
    fpCfg.xSize = (XLONG) 2148;
    fpCfg.ySize = (XLONG) 4096;

    (void) dhsOpenConnect(&istat, resp, (dhsHandle *) & nocs_dhsID,
			  IamNOCS, &fpCfg);
    if (istat < 0) {
	fprintf(stderr, "ERROR: dhsOpenConnect failed. \\\\ %s\n", resp);
	exit(1);		/* exit for now */
    }
    printf("=================end nocs dhsOpenConnect=================\n");


    /* set up for an AV Pair header write
     */
    mdCfg.metaType     = DHS_MDTYPE_AVPAIR;
    mdCfg.numFields    = DHS_AVP_NUMFIELDS;
    mdCfg.fieldSize[0] = (XLONG) DHS_AVP_NAMESIZE;
    mdCfg.fieldSize[1] = (XLONG) DHS_AVP_VALSIZE;
    mdCfg.fieldSize[2] = (XLONG) DHS_AVP_COMMENT;
    mdCfg.dataType[0]  = (XLONG) DHS_UBYTE;
    mdCfg.dataType[1]  = (XLONG) DHS_UBYTE;
    mdCfg.dataType[2]  = (XLONG) DHS_UBYTE;


    /* 
    expID = 2454100.0 + ((double) random() / (double) RAND_MAX * 100.);
     */
    expID = 2454100.0;


    /* Leave this here as a default, now updated for each image
     */

    /*  AV Pair Header.  We create this for each image so we can verify
     **  we grabbed the right metadata pages, i.e. the keyword values will
     **  be "image_<expnum>_<keywnum>".  Likewise, the image array will
     **  have reference pixels that are "4000+<expnum>".
     */
    op = mdBuf;
    for (i = 0; Header[i].keyw; i++) {
	memset(keyw, ' ', 32);
	memset(val, ' ', 32);
	memset(comment, ' ', 64);

        sprintf(keyw, "%s", trim (Header[i].keyw));
        sprintf(val, "%s", trim (Header[i].val));
        sprintf(comment, "%s", trim (Header[i].comment));

	memmove(op, keyw, 32);
	memmove(&op[32], val, 32);
	memmove(&op[64], comment, 64);
	op += 128;
	nkeys++;
    }
    blkSize = (size_t) (nkeys * 128);

    printf("================= MSG Created %d header keywords ....\n", nkeys);


/* leave fpCfg here, but interaction is now inside the loop
 * to communicate from EXP through to PAN
 */

/*    fpCfg.xStart = (XLONG) 0;
 *    fpCfg.yStart = (XLONG) 0;
 *    fpCfg.xSize = ncols;
 *    fpCfg.ySize = nrows;
 */

/*    fpCfg.xStart = (XLONG) 864;
 *   fpCfg.yStart = (XLONG) 0;
 *   fpCfg.xSize = (XLONG) 260;
 *   fpCfg.ySize = (XLONG) 2048;
 */

/*    fpCfg.xStart = (XLONG) 512;
 *    fpCfg.yStart = (XLONG) 1536;
 *    fpCfg.xSize = (XLONG) 612;
 *    fpCfg.ySize = (XLONG) 1024;
 */

    fpCfg.xStart = (XLONG) 0;
    fpCfg.yStart = (XLONG) 0;
    fpCfg.xSize = (XLONG) 2148;
    fpCfg.ySize = (XLONG) 4096;

    /* defaults in case caller (exp.c) doesn't provide the info in msg
     */
    strcpy (nocroipt, "FullFrame");
    strcpy (rcode, "4k");
    strcpy (bcode, "1x1");

    printf("================= MSG waiting for commands.....\n");
    to_tid = subject = -1;
    while (mbusRecv(&from_tid, &to_tid, &subject, &host, &msg) >= 0) {

	/* We don't readlly care about the value, we just want a 
	 ** monotonically increasing number.....
	expID += 0.01;
	 */
	/* expID = atof (msg);
	 */

        /* now we care about the value as a side channel for ROI/binning
         * info between NOCS and PAN simulators
         */
        nscan = sscanf (msg, "%lf %s %s", &expID, rcode, bcode);
        dhs.expID = expID;

	fprintf (stderr, "%d %s\n", nscan, msg);

        if (nscan >= 2) {       
            if (strcmp (rcode, "320") == 0) {
                fprintf (stderr, "rcode=320\n");
		strcpy (nocroipt, "320x4k");
                x_roi = 320; y_roi = 4096;      

            } else if (strcmp (rcode, "1k") == 0) {
                fprintf (stderr, "rcode=1k\n");
		strcpy (nocroipt, "1kx1k");
                x_roi = 1024; y_roi = 1024;
    
            } else if (strcmp (rcode, "2k") == 0) {
                fprintf (stderr, "rcode=2k\n");
		strcpy (nocroipt, "2kx2k");
                x_roi = 2048; y_roi = 2048;

            } else {    /* default to 4k */     
                fprintf (stderr, "rcode=4k\n");
		strcpy (nocroipt, "FullFrame");
                x_roi = 2048; y_roi = 4096;
            }                   
        }
    
        if (nscan >= 3) {
            if (strcmp (bcode, "2x2") == 0) {
                fprintf (stderr, "bcode=2x2\n");
                nbin1 = 2; nbin2 = 2;
                                   
            } else if (strcmp (bcode, "2x1") == 0) {
                fprintf (stderr, "bcode=2x1\n");
                nbin1 = 2; nbin2 = 1;
                                 
            } else {    /* default to 1x1 */
                fprintf (stderr, "bcode=1x1\n");
                nbin1 = 1; nbin2 = 1;
            }
        }

        if (lbnl) {     /* needs review should LBNL be deployed */
            ncols     = 2048;
            nrows     = 4096;
        } else {      
            ncols     = x_roi / nbin1 + 100;
            nrows     = y_roi / nbin2;
        }
           
        xstart = (2048 - x_roi) / 2;         ystart = (4096 - y_roi) / 2;

        fprintf (stderr, "ncols: %d  nrows: %d  bin:%dx%d\n", ncols, nrows, nbin1, nbin2);

        fpCfg.xStart = (XLONG) xstart;
        fpCfg.yStart = (XLONG) ystart;
        fpCfg.xSize = ncols;
        fpCfg.ySize = nrows;

	/*  AV Pair Header.  We create this for each image so we can verify
	 **  we grabbed the right metadata pages, i.e. the keyword values will
	 **  be "image_<expnum>_<keywnum>".  Likewise, the image array will
	 **  have reference pixels that are "4000+<expnum>".
	 */
	nkeys = 0;	/* reset */
	op = mdBuf;
	for (i = 0; Header[i].keyw; i++) {
	    memset(keyw, ' ', 32);
	    memset(val, ' ', 32);
	    memset(comment, ' ', 64);

	    sprintf(keyw, "%s", trim (Header[i].keyw));
	    sprintf(val, "%s", trim (Header[i].val));
	    sprintf(comment, "%s", trim (Header[i].comment));

	    if (strncmp(keyw, "NOCROIRZ", 8) == 0) {
		sprintf(val, "%d", y_roi);
	    } else if (strncmp(keyw, "NOCROIRS", 8) == 0) {
		sprintf(val, "%d", ystart);
	    } else if (strncmp(keyw, "NOCROICZ", 8) == 0) {
		sprintf(val, "%d", x_roi / 2);
	    } else if (strncmp(keyw, "NOCROICS", 8) == 0) {
		sprintf(val, "%d", xstart);
	    } else if (strncmp(keyw, "NOCROIPT", 8) == 0) {
		sprintf(val, "%s", nocroipt);
	    } else if (strncmp(keyw, "NOCROI", 6) == 0) {
		sprintf(val, "%s", "enabled");
	    } else if (strncmp(keyw, "NOCRBIN", 7) == 0) {
		sprintf(val, "%d", nbin2);
	    } else if (strncmp(keyw, "NOCCBIN", 7) == 0) {
		sprintf(val, "%d", nbin1);
	    }

	    memmove(op, keyw, 32);
	    memmove(&op[32], val, 32);
	    memmove(&op[64], comment, 64);
	    op += 128;
	    nkeys++;
	}
	blkSize = (size_t) (nkeys * 128);

	printf("================= MSG subj: %d '%s'\n", subject, msg);
	if (subject == MB_START) {
            char msg[32];

	    printf ("\t%s -- Got a START message\n", pTime(buf));

	    /* Send OpenExp from NOCS
	     */
	    printf
		("=================start nocs dhsOpenExp=================\n");
	    (void) dhsOpenExp(&istat, resp, (dhsHandle) nocs_dhsID, &fpCfg,
			      &expID, obsID);
	    printf
		("=================end nocs dhsOpenExp=================\n");
	    if (istat < 0) {
		printf("ERROR: NOCS: dhsOpenExp failed. \\\\ %s\n", resp);
		istat = 1;
		break;
	    }

	    /* Send PRE META from the NOCS.
	     */
	    printf
		("============== start nocs dhsSendMetaData ==============\n");

	    if (!sim)
	        dhsSendMetaData(&istat, retStr, (dhsHandle) nocs_dhsID,
			        (char *)mdBuf, blkSize, &mdCfg, &expID, obsID);
    	    printf
		("================= MSG Sent %d bytes (%d/%d keywords) ....\n",
		    blkSize, (blkSize / 128), nkeys);
	    printf
		("=================end nocs dhsSendMetaData=================\n");

	    if (istat < 0) {
		printf("ERROR: NOCS 2nd dhsSendMetaData failed. \\\\ %s\n",
		       retStr);
		istat = 1;
		break;
	    }

            /* Tell the PANs to start running.
             */
	    memset (msg, 0, 32);
            sprintf (msg, "%.6lf %s %s", expID, rcode, bcode);
            mbusBcast("PanA", msg, MB_START);

	    printf ("================= MSG waiting for FINISH.....\n");


	} else if (subject == MB_FINISH) {
            char msg[32];

	    printf ("\t%s -- Got a FINISH message\n", pTime(buf));

	    /* Skip until we get a FINISH from each of the PANs
	    fcount++;
	    if ((fcount % n_pans) == 0) {
		to_tid = subject = -1;
		continue;
	    }
	    */

	    /* Send POST META from NOCS
	     */
	    printf
		("=================start nocs dhsSendMetaData=================\n");
	    if (!sim)
	        dhsSendMetaData(&istat, retStr, (dhsHandle) nocs_dhsID,
			        (char *)mdBuf, blkSize, &mdCfg, &expID, obsID);
    	    printf
		("================= MSG Sent %d bytes (%d/%d keywords) ....\n",
		    blkSize, (blkSize / 128), nkeys);
	    printf
		("=================end nocs dhsSendMetaData=================\n");
	    if (istat < 0) {
		printf("ERROR: NOCS 2nd dhsSendMetaData failed. \\\\ %s\n",
		       retStr);
		istat = 1;
		break;
	    }

	    /* Send CloseExp from NOCS.  This is supposed to trigger the 
	     ** processing in the Supervisor.
	     */
	    printf
		("=================start nocs dhsCloseExp=================\n");
	    (void) dhsCloseExp(&istat, resp, (dhsHandle) nocs_dhsID,
			       expID);
	    printf
		("=================end nocs dhsCloseExp=================\n\n");
	    if (istat < 0) {
		printf("ERROR: NOCS: dhsCloseExp failed. \\\\ %s\n", resp);
		istat = 1;
		break;
	    }

	    memset (msg, 0, 32);
            sprintf (msg, "%.6lf", expID);
            mbusBcast("trigger", msg, MB_FINISH);

	} else
	    printf("Unknown message: %d\n", subject);


	if (subject != MB_START) {
	    printf ("\n\n");
	    printf ("================= MSG waiting for commands.....\n");
	}

	to_tid = subject = -1;

        if (host) free ((void *) host);
        if (msg)  free ((void *) msg);
    }


    printf("=================start dhsCloseConnect=================\n");
    dhsCloseConnect(&istat, retStr, (dhsHandle) dhsID);
    printf("=================end dhsCloseConnect=================\n");

    free(p);
    printf("=================Closing COLLECTOR socket=================\n");


    mbusDisconnect (mytid);

    exit(istat);
}



static char *
trim (char *buf)
{
    char *ip = buf;

    while (*ip && *ip == ' ')
        ip++;

    return (ip);
}


char *pTime(char *inStr)
{
    /* declare some variables and initialize them */
    struct tm *tim;
    time_t t;

    t = time((time_t *) & t);
    tim = localtime((time_t *) & t);

    if (inStr == (char *) NULL) {
	return inStr;
    }

    sprintf(inStr, "%4d%02d%02dT%02d%02d%02d-", tim->tm_year + 1900,
	    tim->tm_mon + 1, tim->tm_mday, tim->tm_hour, tim->tm_min,
	    tim->tm_sec);

    return inStr;
}
