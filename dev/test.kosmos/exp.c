/**
 *
 */
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "mbus.h"


/*  Client program to generate a fake exposure to the DHS simulator.
**  All we really do here is connect to the message bus and issue a
**  START command to the nocs/pan simulators.
*/


int loop 	= 1;
int verbose 	= 1;
int delay 	= 15;
int interactive = 0;
int lbnl 	= 0;		/* use the 4k x 4k LBNL chip	*/

char  bin[SZ_FNAME];
char  roi[SZ_FNAME];

void   sendExposure (char *group, double expID);
char  *pTime(char *buf);


int 
main(int argc, char *argv[])
{
    int    i, nscan, mytid, expnum = 0;
    int    x_roi, y_roi, nbin1, nbin2, ncols=2148, nrows=4096;
    char  *group = "trigger";
    char  tbin[SZ_FNAME];
    char  troi[SZ_FNAME];
    char   buf[SZ_FNAME], msg[SZ_FNAME], cmd[SZ_FNAME];
    double expID = 2450100.0;

/* move this after the command-line arguments */
    /* Initialize connections to the message bus.
    */
/*    if ((mytid = mbusConnect("EXP", group, FALSE)) <= 0) {
 *	fprintf(stderr, "ERROR: Can't connect to message bus.\n");
 *	exit(1);
 *   }
 */

    strcpy (bin, "1x1");
    strcpy (roi, "4k");


    /* Process the command-line arguments.
     */
    for (i = 1; i < argc; i++) {
	if (strncmp(argv[i], "-help", 2) == 0) {
	    fprintf (stderr,
		"Usage: exp [-loop <N>] [-delay <nsec>] [-interactive]\n");
	    exit(0);;

	} else if (strncmp(argv[i], "-bin", 2) == 0) {
	    strcpy (bin, argv[++i]);

	    if (strncmp(bin,"1x1",3)!=0 && strncmp(bin,"2x1",3)!=0 &&
		strncmp(bin,"2x2",3)!=0) {

		fprintf (stderr, "-bin must be one of 1x1, 2x1 or 2x2\n");
		exit(1);
	    }

	} else if (strncmp(argv[i], "-roi", 2) == 0) {
	    strcpy (roi, argv[++i]);

	    if (strncmp(roi,"4k",3)!=0 && strncmp(roi,"2k",3)!=0 &&
		strncmp(roi,"1k",3)!=0 && strncmp(roi,"320",3)!=0) {

		fprintf (stderr, "-roi must be one of 4k, 2k, 1k or 320\n");
		exit(1);
	    }

	} else if (strncmp(argv[i], "-group", 2) == 0) {
	    group = argv[++i];

	} else if (strncmp(argv[i], "-expid", 2) == 0) {
	    i++;		/* no-op */

	} else if (strncmp(argv[i], "-lbnl", 3) == 0) {
	    lbnl++;		/* unimplemented */

/*	} else if (strncmp(argv[i], "-start", 2) == 0) {
 *	    sprintf (msg, "%.4f", (expID += 0.1));
 *	    mbusBcast(group, msg, MB_START);
 */

	} else if (strncmp(argv[i], "-delay", 2) == 0) {
	    delay = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-interactive", 2) == 0) {
	    interactive++;
	    loop += 999;

	} else if (strncmp(argv[i], "-loop", 3) == 0) {
	    loop = atoi(argv[++i]);

	} else if (strncmp(argv[i], "-verbose", 2) == 0) {
	    verbose++;
	
	} else {
	    fprintf (stderr, "ERROR: Unknown flag '%s'\n", argv[i]);
	    exit(1);
	}
    }

/*    if (verbose)
 */
	printf ("\n  ROI = %s, BIN = %s\n", roi, bin);
	printf ("delay = %d, loop = %d\n", delay, loop);

    /* Initialize connections to the message bus.
    */
    if ((mytid = mbusConnect("EXP", group, FALSE)) <= 0) {
	fprintf(stderr, "ERROR: Can't connect to message bus.\n");
	exit(1);
    }

    if (interactive) {

        /* printf ("\n\n\n");
	 */
        while (1) {
            printf ("\n[ROI [BIN]] <CR> to start, or loop|all [#], q=quit: ");

            if (fgets (buf, 80, stdin) == NULL || buf[0] == '\n') {
		printf ("\n");

            } else if (buf[0] == 'l') {
		nscan = sscanf(buf, "%s %d\n", cmd, &loop);

		if (nscan != 2)
		    loop = 1;

		printf ("\n  %d @ ROI = %s, BIN = %s, [%dx%d]\n\n",
		    loop, roi, bin, ncols, nrows);

		expnum = 0;
		for (i=1; i <= loop; i++) {
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);

		    sendExposure ("NOCS", expID);

		    if (delay > 0)
			sleep (delay);	
		}

		continue;

            } else if (buf[0] == 'a') {
		nscan = sscanf(buf, "%s %d\n", cmd, &loop);

		if (nscan != 2)
		    loop = 1;

		expnum = 0;
		for (i=1; i <= loop; i++) {
		    strcpy (roi, "4k"); strcpy (bin, "1x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "4k"); strcpy (bin, "2x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "4k"); strcpy (bin, "2x2");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "2k"); strcpy (bin, "1x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "2k"); strcpy (bin, "2x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "2k"); strcpy (bin, "2x2");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "320"); strcpy (bin, "1x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "320"); strcpy (bin, "2x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "320"); strcpy (bin, "2x2");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "1k"); strcpy (bin, "1x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "1k"); strcpy (bin, "2x1");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	

		    strcpy (roi, "1k"); strcpy (bin, "2x2");
		    printf ("ROI = %s, BIN = %s\n", roi, bin);
		    sprintf (msg, "%.4f", (expID += 0.1));
		    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);
		    sendExposure ("NOCS", expID);
		    if (delay > 0) sleep (delay);	
		}

		continue;

            } else if (buf[0] == 'q') {
                break;

            } else if (buf[0] == 'h' || buf[0] == '?' || buf[0] == 'H') {
		printf ("\n  ROI = %s, BIN = %s\n\n", roi, bin);
		continue;

	    } else {	/* setting ROI and binning */
		nscan = sscanf(buf, "%s %s\n", troi, tbin);

		if (nscan < 1 || nscan > 2)
		    continue;

		/* if (buf[0]!='1' && buf[0]!='2'&& buf[0]!='3' && buf[0]!='4')
		 */

		if (buf[0] == '1')		/* for quick typing */
		    strcpy (troi, "1k");
		else if (buf[0] == '2')
		    strcpy (troi, "2k");
		else if (buf[0] == '3')
		    strcpy (troi, "320");
		else if (buf[0] == '4')
		    strcpy (troi, "4k");
		else
		    fprintf (stderr, "unknown command, ");


		if (strncmp(troi,"4k",3)!=0 && strncmp(troi,"2k",3)!=0 &&
		    strncmp(troi,"1k",3)!=0 && strncmp(troi,"320",3)!=0) {

		    fprintf (stderr, "ROI must be one of 4k, 2k, 1k or 320\n");
		    continue;
		}

		if (nscan == 2) {
		    if (tbin[0] == '1')		/* for quick typing */
			strcpy (tbin, "1x1");
		    else if (tbin[0] == '2')
			strcpy (tbin, "2x1");
		    else if (tbin[0] == '4')	/* do the multiplication */
			strcpy (tbin, "2x2");

		    if (strncmp(tbin,"1x1",3)!=0 && strncmp(tbin,"2x1",3)!=0 &&
			strncmp(tbin,"2x2",3)!=0) {

			fprintf (stderr, "BIN must be one of 1x1, 2x1 or 2x2\n");
			continue;
		    }

		    strcpy (bin, tbin);
		}

		strcpy (roi, troi);

		if (strcmp (roi, "320") == 0) {
		    x_roi = 320; y_roi = 4096;
		} else if (strcmp (roi, "1k") == 0) {
		    x_roi = 1024; y_roi = 1024;
		} else if (strcmp (roi, "2k") == 0) {
		    x_roi = 2048; y_roi = 2048;
		} else {    /* default to 4k */
		    x_roi = 2048; y_roi = 4096;
		}

	       if (strcmp (bin, "2x2") == 0) {
		    nbin1 = 2; nbin2 = 2;
		} else if (strcmp (bin, "2x1") == 0) {
		    nbin1 = 2; nbin2 = 1;
		} else {    /* default to 1x1 */
		    nbin1 = 1; nbin2 = 1;
		}

		ncols = x_roi / nbin1 + 100;
		nrows = y_roi / nbin2;

		printf ("\n  ROI = %s, BIN = %s, %dx%d\n\n", roi, bin, ncols, nrows);

	    }

	    sprintf (msg, "%.4f", (expID += 0.1));
	    printf ("  exp %d:  ID %.2lf ", ++expnum, expID);

	    sendExposure ("NOCS", expID);

/*	    if (delay > 0)
 *	        sleep (delay);	
 */
	}

    } else {
        for (i=1; i <= loop; i++) {
	    fprintf (stderr, "%s Exp #%d  ID:%.2lf \n",
		pTime(buf), i, (expID += 0.1));

	    sendExposure ("NOCS", expID);

/*	    if (delay > 0)
 *	        sleep (delay);	
 */
        }
    }

    /* Disconnect from the message bus.
    */
/*    if (interactive) {
 *       printf ("Hit <cr> to quit ....\n");
 *       if (fgets (buf, 80, stdin) == NULL || buf[0] == '\n')
 *	    ;
 *   }
 */

    mbusDisconnect (mytid);

    return (0);
}


void
sendExposure (char *group, double expID)
{
    int from_tid = -1, to_tid = -1, subject = -1;
    char *host, msg[SZ_LINE], *m = msg;


    bzero (msg, SZ_LINE);
    sprintf (msg, "%.4lf %s %s\n", expID, roi, bin);
    mbusBcast (group, msg, MB_START); 	/* start the exposure     */

    printf("...waiting for completion ");
    fflush(stdout);

    if (delay > 0)
	sleep (delay);	

    while (mbusRecv(&from_tid, &to_tid, &subject, &host, &m) >= 0) {
	if (subject == MB_FINISH)
	    break;
    }
    printf("...done\n");

    if (host) free ((void *) host);
}


char *pTime(char *inStr)
{
    /* declare some variables and initialize them */
    struct tm *tim;
    time_t t;

    t = time((time_t *) & t);
    tim = localtime((time_t *) & t);

    if (inStr == (char *) NULL) {
	return inStr;
    }

    sprintf(inStr, "%4d%02d%02dT%02d%02d%02d-", tim->tm_year + 1900,
	    tim->tm_mon + 1, tim->tm_mday, tim->tm_hour, tim->tm_min,
	    tim->tm_sec);

    return inStr;
}
