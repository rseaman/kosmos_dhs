# Extensions Names

switch $nimages {
 1 { set extname(ccd1,A) im1 }
 2 { set extname(ccd1,A) im1
     set extname(ccd1,B) im2 }
}

# Detectors

set detname(mos)  "KOSMOS"
set detname(ccd1) "K123-45"

# Default detector values if not found in ucd file.
set detval(gain,approx) 0.6
set detval(rdnoise,approx) 5.5
set detval(saturate,approx) 200000

# Geometry.
# This is unbinned.  The KTM will adjust these based only on binning factors.

set detsize(mos,1) 2048; set detsize(mos,2) 4096
set detsize(ccd,1) 2048; set detsize(ccd,2) 4096
set detsize(bias,1) 50
switch $nimages {
 1 { set detsize(amp,1) 1024; set detsize(amp,2) 4096 }
 2 { set detsize(amp,1) 1024; set detsize(amp,2) 4096 }
}

# CCD to Image transformation (IRAF logical).
set ltv(1) 0; set ltv(2) 0; set ltm(1) 1; set ltm(2) 1

# CCD to Amplifier transformation.
set atv(A,1)    0; set atv(A,2)    0; set atm(A,1)  1; set atm(A,2)  1
set atv(B,1) 2049; set atv(B,2)    0; set atm(B,1) -1; set atm(B,2)  1

# CCD to Detector transformation.
set dtv(ccd1,1)    0; set dtv(ccd1,2)    0; set dtm(ccd1,1) 1; set dtm(ccd1,2) 1

# Default astrometery.

# Longslit -- This can be later generalized to 3D WCS.
#set wc(b2k) 5216; set dw(b2k) 0.69
#set wc(r2k) 7600; set dw(r2k) 1.01
set WCSASTRM(ccd1) {Placeholder WCS}
set CTYPE1(ccd1) LINEAR; set CTYPE2(ccd1) LINEAR
set CRVAL1(ccd1) 0.0;    set CRVAL2(ccd1) 5216
set CRPIX1(ccd1) 1024.5; set CRPIX2(ccd1) 2048.5
set CD1_1(ccd1) 0.29;    set CD2_2(ccd1) 0.69
set CD1_2(ccd1) 0.0;     set CD2_1(ccd1) 0.0

#set WCSASTRM(kp4m,ccd1) {Placeholder WCS}
#set CTYPE1(kp4m,ccd1) RA---TNX
#set CTYPE2(kp4m,ccd1) DEC--TNX
#set CRPIX1(kp4m,ccd1) 1024.5
#set CRPIX2(kp4m,ccd1) 2048.5
#set CD1_1(kp4m,ccd1) 8.06E-5
#set CD2_2(kp4m,ccd1) 8.06E-5
#set CD1_2(kp4m,ccd1) 0.0
#set CD2_1(kp4m,ccd1) 0.0

# Default when there is no astrometry.
#set WCSASTRM(ccd1) {None available}
#set CTYPE1(ccd1)  LINEAR; set CTYPE2(ccd1)  LINEAR
#set CRVAL1(ccd1)  1024.5; set CRVAL2(ccd1)  2048.5
#set CRPIX1(ccd1)  1024.5; set CRPIX2(ccd1)  2048.5
#set CD1_1(ccd1) 1.; set CD2_2(ccd1) 1.; set CD1_2(ccd1) 0.; set CD2_1(ccd1) 0.

# Aperture descriptions based on NOCSLIT value and slit wheel.

set slitwheel(2px) "slit 0.6x100arcsec"
set slitwheel(3px) "slit 0.9x100arcsec"
set slitwheel(3pxD) $slitwheel(3px)
set slitwheel(3pxU) $slitwheel(3px)
set slitwheel(5mu) "holearray 5mu"
